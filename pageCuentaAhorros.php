<?php
include_once('utils/menuNavegacion.php');
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
	<?php
	$ur = new UsuariosRepositorio();
	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual != null && $usuario_actual->tieneCuentasAhorros()) {
		echo "<link rel='stylesheet' type='text/css' href='assets/tablas/style.css'>";
	}
	?>
	<title></title>
</head>

<body>
	<?php
	echo menuNavegacion::getMenu(false);

	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual == null || get_class($usuario_actual) == 'Visitante') {
		header("Location: login.php");
	}
	?>
	<main class="container">
		<h1>Cuenta de Ahorros</h1>
		<div>Servicio de ahorros para almacenar tus JaveCoins sin restricciones.</div>
		<div>
			<?php
			$html = "";
			if ($usuario_actual != null && $usuario_actual->tieneCuentasAhorros()) {
				$html .= "<h2>Cuentas de Ahorro Actuales</h2>";
				$html .= "<p></p>";
				$html .= "<p></p>";
				$html .= "<p></p>";
				$html .= "<table border='1' style='margin: auto'>";
				$html .= "<tr>";
				$html .=   "<th>ID</th>";
				$html .=   "<th>Saldo</th>";
				$html .=   "<th>Cuota de Manejo</th>";
				$html .= "</tr>";

				foreach ($usuario_actual->obtenerCuentasAhorros() as $fila) {
					$html .= "<tr>";
					$html .=   "<td>"   . $fila['ID']           . "</td>";
					$html .=   "<td>\$" . $fila['Saldo']        . "</td>";
					$html .=   "<td>\$" . $fila['Cuota_manejo'] . "</td>";
					$html .= "</tr>";
				}
				$html .= "</table>";
				echo $html;
			}
			?>
		</div>
		<h2>Adquirir producto</h2>
			<h2>
				<p>
					<a class='regresar' href='pageCuentaAhorrosAdquirir.php'>Adquirir ahora</a>
				</p>
			</h2>
	</main>

</body>

</html>