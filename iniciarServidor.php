<?php
include_once("config.php");
include_once("repository/database.php");
include_once("repository/DatabaseInit.php");
include_once("repository/UsuariosRepositorio.php");
include_once("repository/MensajesRepositorio.php");
include_once("repository/ProductosRepositorio.php");
include_once("repository/BancosRepositorio.php");
include_once("model/usuarios/Administrador.php");
include_once("model/banco/Banco.php");
?>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <title></title>
</head>

<body>
    <h3> Eliminación sesión
    <?php
        session_start();
        $_SESSION['userID'] = null;
    ?>
    </h3>
    <h3>
        <?php
        Database::eliminar_Database();
        ?>
    </h3>
    <h3>
        <?php
        $ur = new DatabaseInit();
        $ur->create_tables();
        ?>
    </h3>
    <h3> Creacion usuario admin, cliente y visitante  
        <?php
            $ur = new BancosRepositorio();
            $ur->agregar(new Banco(Banco::ID, "BANCO DAVAVANDA"));
            $ur->agregar(new Banco(Banco::ID, "BANCO PIPUL"));
            $ur = new UsuariosRepositorio();
            $ur->agregar(new Administrador(Usuario::ID, "adminN", "adminA", "admin@admin.com", "admin@admin.com", "admin", 1));
            $ur->agregar(new Cliente(Usuario::ID, "clienteN", "clienteA", "cliente@cliente.com", "cliente@cliente.com", "cliente", 1));
            $ur->agregar(new Visitante(Usuario::ID, "visitanteN", "visitanteA", "visitante@visitante.com", 1));
            echo "\nUsuarios Predefinidos creados correctamente";
            $ur = new ProductosRepositorio();
            $ur->agregar(new Credito(Producto::ID, 3, 50.5, date("2000-11-11"), false, 20)); //TODO -> en tasa de interes se deberá poner la que pone el administrador en este banco
            $ur->agregar(new TarjetaCredito(Producto::ID, 3, 150, 160, 5, 10));
            $ur->agregar(new CuentaAhorro(Producto::ID, 3, 100, 10));
        ?>
    </h3>
    <p>
        <a class="regresar" href="menuPruebas.php"><input type="button" value="Menu Pruebas" id="form_button"></a>
    </p>
</body>

</html>
