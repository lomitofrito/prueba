<?php
include_once('utils/menuNavegacion.php');
include_once("repository/ProductosRepositorio.php");
include_once("model/usuarios/Administrador.php");
include_once("model/banco/Producto.php");
include_once("model/banco/Credito.php");
include_once("config.php");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
    <title></title>
</head>

<body>
    <?php
    echo menuNavegacion::getMenu(false);
    ?>
    <main class="container">
        <?php
        $ur = new ProductosRepositorio();
        $confirmacion = false;
        if (!empty($_POST['saldo']) && !empty($_POST['fecha_pago']) && !empty($_POST['id_usuario'])) {
            if (empty($_POST['interes'])) { // Proceso para visitante
                $confirmacion = $ur->agregar(new Credito(Producto::ID, $_POST['id_usuario'], $_POST['saldo'], $_POST['fecha_pago'], false, 20)); //TODO -> en tasa de interes se deberá poner la que pone el administrador en este banco
            } else {
                $confirmacion = $ur->agregar(new Credito(Producto::ID, $_POST['id_usuario'], $_POST['saldo'], $_POST['fecha_pago'], false, $_POST['interes']));
            }
        }
        if ($confirmacion) {
            echo "<p>Solicitud de crédito creada correctamente</p>";
        } else {
            echo "<p>Error en la Solicitud de crédito, intente nuevamente<p>";
        }
        ?>
    </main>
</body>

</html>