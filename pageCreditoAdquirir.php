<?php
include_once('utils/menuNavegacion.php');
include_once("repository/UsuariosRepositorio.php");
include_once("model/usuarios/Visitante.php");
include_once("model/usuarios/Usuario.php");
include_once("config.php");

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
    <title></title>
</head>

<body>
    <?php
    echo menuNavegacion::getMenu(false);
    ?>
    <main class="container">
        <?php
        $ur = new UsuariosRepositorio();
        $html = "";
        $html .= "<form action='pageCreditoProcesar.php' method='POST'>";

        if (!empty($_POST['nombre']) && !empty($_POST['apellido']) && !empty($_POST['email'])) {
            $usuario_encontrado = $ur->obtener_usuario_por_correo($_POST['email']);
            if ($usuario_encontrado == null) {
                $resultado = $ur->agregar(new Visitante(Usuario::ID, $_POST['nombre'], $_POST['apellido'], $_POST['email']));
                if ($resultado) {
                    $usuario_encontrado = $ur->obtener_usuario_por_correo($_POST['email']);
                }
                else{
                    header("Location: pageCredito.php");
                }
            }

            //Código para Visitante
            $html .= "<input name='saldo' type='number' step='any' min='0' placeholder='Ingrese el monto del crédito necesitado'>";
            $html .= "<input name='fecha_pago' type='date' placeholder='Ingrese la fecha de pago'>";
            $html .= "<input name='id_usuario' value='$usuario_encontrado->id' type='hidden'>";
        
        } else {
            $usuario_actual = $ur->getUsuarioActual(true);

            //Código para Cliente
            $html .= "<input name='interes' type='number' min='0' max='100' size='6' placeholder='Ingrese la tasa de interés que quiere pagar'>";
            $html .= "<input name='saldo' type='number' step='any' min='0' placeholder='Ingrese el monto del crédito necesitado'>";
            $html .= "<input name='fecha_pago' type='date' placeholder='Ingrese la fecha de pago'>";
            $html .= "<input name='id_usuario' value='$usuario_actual->id' type='hidden'>";
        }
        
        $html .= "	<input type='submit' value='Enviar Solicitud'>";
        $html .= "</form>";
        echo $html;
        ?>
    </main>
</body>

</html>