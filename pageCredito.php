<?php
include_once('utils/menuNavegacion.php');
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
	<?php
	$ur = new UsuariosRepositorio();
	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual != null && $usuario_actual->tieneCreditos()) {
		echo "<link rel='stylesheet' type='text/css' href='assets/tablas/style.css'>";
	}
	?>
	<title></title>
</head>

<body>
	<?php
	echo menuNavegacion::getMenu(false);

	$usuario_actual = $ur->getUsuarioActual(true);
	?>
	<main class="container">
		<h1>CRÉDITO</h1>
		<div>Servicio de crédito con propuestas de tasa de interés de parte del usuario.</div>
		<?php
			$html = "";
			if($usuario_actual != null && $usuario_actual->tieneCreditos())
			{
				$html .= "<h2>Créditos Actuales</h2>";
				$html .= "<p></p>";
				$html .= "<p></p>";
				$html .= "<p></p>";
				$html .= "<table border='1' style='margin: auto'>";
				$html .= "<tr>";
				$html .=   "<th>ID</th>";
				$html .=   "<th>Saldo</th>";
				$html .=   "<th>Fecha de pago</th>";
				$html .=   "<th>Estado de Aprobación</th>";
				$html .=   "<th>Tasa de interés</th>";
				$html .= "</tr>";

				foreach ($usuario_actual->obtenerCreditos() as $fila) {
					$html .= "<tr>";
					$html .=   "<td>"   . $fila['ID']           . "</td>";
					$html .=   "<td>\$" . $fila['Saldo']        . "</td>";
					$html .=   "<td>"   . $fila['Fecha_pago']   . "</td>";
					$html .=   "<td>"   . ($fila['Aprobado'] == 0 ? "No aprobado" : 'Aprobado') . "</td>";
					$html .=   "<td>"   . $fila['Tasa_interes'] . "%</td>";
					$html .= "</tr>";
				}
				$html .= "</table>";
				echo $html;
			}
			else{
				$html .= "<h2>Adquirir producto</h2>";
				if ($usuario_actual == null || get_class($usuario_actual) == 'Visitante') {
					$html .= "<p>";
					$html .= "	<a class='regresar' href='login.php'>Inicie Sesión </a>";
					$html .= "	O realice el siguiente formulario:";
					$html .= "</p>";
					$html .= "<form action='pageCreditoAdquirir.php' method='POST'>";
					$html .= "	<input name='nombre' type='text' placeholder='Enter your name'>";
					$html .= "	<input name='apellido' type='text' placeholder='Enter your apellido'>";
					$html .= "	<input name='email' type='email' placeholder='Enter your email'>";
					$html .= "	<input type='submit' value='Submit'>";
					$html .= "</form>";
				}
				else{
					$html .= "<h2><p><a class='regresar' href='pageCreditoAdquirir.php'>Adquirir ahora</a></p></h2>";
				}
				echo $html;
			}
		?>
	</main>

</body>

</html>