<?php
include_once('utils/menuNavegacion.php');
include_once('repository/ProductosRepositorio.php');
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
	<?php
	$ur = new UsuariosRepositorio();
	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual != null && $usuario_actual->tieneCuentasAhorros()) {
		echo "<link rel='stylesheet' type='text/css' href='assets/tablas/style.css'>";
	}
	?>
	<title></title>
</head>

<body>
	<?php
	echo menuNavegacion::getMenu(false);

	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual == null || get_class($usuario_actual) == 'Visitante') {
		header("Location: login.php");
	}
	?>
	<main class="container">
		<h1>Retiros</h1>
		<div>Servicio de retiro de Javecoins.</div>

		<?php
		$html = "";
		if ($usuario_actual != null && $usuario_actual->tieneCuentasAhorros()) {

			$html .= "<table border='1' style='margin: auto'>";
			$html .= "<tr>";
			$html .=   "<th>ID</th>";
			$html .=   "<th>Saldo</th>";

			$html .= "</tr>";

			foreach ($usuario_actual->obtenerCuentasAhorros() as $fila) {
				$html .= "<tr>";
				$html .=   "<td>" . $fila['ID']       . "</td>";
				$html .=   "<td>" . $fila['Saldo']       . "</td>";

				$html .= "</tr>";
			}
			echo $html;
		} else {
			echo "<h2><p><a class='regresar' href='pageCuentaAhorrosAdquirir.php'>Adquirir ahora</a></p></h2>";
		}
		?>
		<form name="form" action="" method="post">
			<p> id de Cuenta</p>
			<input type="number" name="cuenta" id="cuenta" value="digite el numero de cuenta a retirar">
			<p>Valor</p>
			<input type="number" name="valor" id="valor" value="digite el valor a retirar">
			<input type="submit" name="submit">
		</form>
		<?php
		if (isset($_POST['cuenta']) && isset($_POST['valor'])) {
			$idCuenta = $_POST['cuenta'];
			$valor = $_POST['valor'];


			$productosRepo = new ProductosRepositorio();
			$usuariosRepo = new UsuariosRepositorio();

			$usuario_actual = $usuariosRepo->getUsuarioActual(true);
			$saldo = 0;
			foreach ($usuario_actual->obtenerCuentasAhorros() as $fila) {
				if ($fila['ID'] == $idCuenta) {
					$saldo = $fila['Saldo'];
				}
			}

			$result = $saldo - $valor;
			if ($valor > 0) {
				if ($result >= 0) {
					echo "<p>Es posible el retiro</p>";
					$resultado = $productosRepo->actualizar(new CuentaAhorro($idCuenta, $usuario_actual->id, $result, 20));
					if ($resultado) {
						echo "<p>Retiro exitoso</p>";
					} else {
						echo "<p>Error en la solicitud de retiro, intente nuevamente<p>";
					}
				} else {
					echo "<p>No hay fondos intente otra cuenta</p>";
				}
			} else echo " introduzca valores";
		}

		?>
		<h2>Cuentas para retirar </h2>
	</main>

</body>

</html>