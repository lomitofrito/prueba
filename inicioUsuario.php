<?php
include_once("repository/UsuariosRepositorio.php");
include_once("model/usuarios/Administrador.php");
include_once("config.php");
?>
<?php
session_start();
$ur = new UsuariosRepositorio();
if (!empty($_POST['email']) && !empty($_POST['password'])) {
	$usuario_encontrado = $ur->obtener_usuario_por_correo($_POST['email']);
	if ($usuario_encontrado == null) {
		header("Location: signUp.php");
	} else {
		$_SESSION['userID'] = $usuario_encontrado->getID();
		echo $ur->obtener_usuario_por_id($_SESSION['userID'])->correo;
		if (get_class($usuario_encontrado) == "Administrador") {
			header("Location: index.php");
		} else if (get_class($usuario_encontrado) == "Cliente") {
			header("Location: index.php");
		} else {
			header("Location: login.php");
		}
	}
} elseif (isset($_SESSION)) {
	$usuario_encontrado = $ur->obtener_usuario_por_id($_SESSION['userID']);
	if (get_class($usuario_encontrado) == "Administrador") {
		header("Location: index.php");
	} else if (get_class($usuario_encontrado) == "Cliente") {
		header("Location: index.php");
	} else {
		header("Location: login.php");
	}
}
//}
?>