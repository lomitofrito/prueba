<?php
include_once('utils/menuNavegacion.php');

session_start();
if (isset($_SESSION['userID'])) {
  header("Location: InicioUsuario.php");
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
  <link rel="stylesheet" type="text/css" href="assets/style.css">
  <title>
    Login
  </title>
</head>

<body>
  <?php
  echo menuNavegacion::getMenu(true);
  ?>
  <main class="container">
    <h1>Login</h1>
    <form action="inicioUsuario.php" method="POST">
      <input name='email' type='email' placeholder='Ingresa tu correo'>
      <input name='password' type='password' placeholder='Ingresa tu contraseña'>
      <input type="submit" value="Submit">
    </form>
    <span>o <a href="signUp.php">Registrate</a></span>
  </main>

</body>

</html>