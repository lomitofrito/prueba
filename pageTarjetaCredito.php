<?php
include_once('utils/menuNavegacion.php');
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
	<?php
	$ur = new UsuariosRepositorio();
	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual != null && $usuario_actual->tieneTarjetasCredito()) {
		echo "<link rel='stylesheet' type='text/css' href='assets/tablas/style.css'>";
	}
	?>
	<title></title>
</head>

<body>
	<?php
	echo menuNavegacion::getMenu(false);

	$usuario_actual = $ur->getUsuarioActual(true);
	if ($usuario_actual == null || get_class($usuario_actual) == 'Visitante') {
		header("Location: login.php");
	}
	?>
	<main class="container">
		<h1>Tarjeta Crédito</h1>
		<div>Servicio de Tarjeta de Crédito para poder realizar las compras que necesite sin más espera. Sujeto a aprobación.</div>

		<?php
			$html = "";
			if ($usuario_actual != null && $usuario_actual->tieneTarjetasCredito()) {
				$html .= "<h2>Tarjetas de Crédito Actuales</h2>";
				$html .= "<p></p>";
				$html .= "<p></p>";
				$html .= "<p></p>";
				$html .= "<table border='1' style='margin: auto'>";
				$html .= "<tr>";
				$html .=   "<th>ID</th>";
				$html .=   "<th>Cuota de Manejo</th>";
				$html .=   "<th>Tasa de interés</th>";
				$html .=   "<th>Cupo Máximo</th>";
				$html .=   "<th>Sobrecupo</th>";
				$html .= "</tr>";

				foreach ($usuario_actual->obtenerTarjetasCredito() as $fila) {
					$html .= "<tr>";
					$html .=   "<td>"   . $fila['ID']           . "</td>";
					$html .=   "<td>\$" . $fila['Cuota_manejo'] . "</td>";
					$html .=   "<td>"   . $fila['Tasa_interes'] . "%</td>";
					$html .=   "<td>\$" . $fila['Cupo_max']     . "</td>";
					$html .=   "<td>\$" . $fila['Sobrecupo']    . "</td>";
					$html .= "</tr>";
				}
				$html .= "</table>";
				echo $html;
			}
		?>
		<h2>Adquirir producto</h2>
		<h2>
			<p>
				<a class='regresar' href='pageTarjetaCreditoAdquirir.php'>Adquirir ahora</a>
			</p>
		</h2>
			
	</main>

</body>

</html>