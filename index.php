<?php
include_once('utils/menuNavegacion.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="assets/menu/main.css">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <title></title>
</head>

<body>
    <?php
		echo menuNavegacion::getMenu(false);
	?>
    <section id="banner">
        <h2>Portal Bancario</h2>
        <p>Manjea tus JaveCoins</p>
        <ul class="actions special">
            <li><a href="signUp.php" class="button primary">Sign Up</a></li>
            <li><a href="login.php" class="button">Login</a></li>
        </ul>
    </section>
</body>

</html>