<?php
include_once('utils/menuNavegacion.php');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="assets/menu/main.css">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <title></title>
</head>

<body>
    <?php
		echo menuNavegacion::getMenu(false);
	?>
    <main class="container">
        <div>
            <h1>Modo Prueba</h1>
            <h2>Antes de iniciar</h2>
            <p>
                <a class="regresar" href="iniciarServidor.php"><input type="button" value="Iniciar Servidor"></a>
            </p>
        </div>
        <div><br><br>
            <h2>Principales</h2>
            <p>
                <a class="regresar" href="signUp.php"><input type="button" value="Sign Up (registrar usuario)"></a>
            </p>
            <p>
                <a class="regresar" href="login.php"><input type="button" value="Login"></a>
            </p>
            <p>
                <a class="regresar" href="logout.php"><input type="button" value="Logout"></a>
            </p>
        </div>
        <div><br><br>
            <h2>Auxiliares</h2>
            <p>
                <a class="regresar" href="pruebaCorreo.php"><input type="button" value="Prueba Correo"></a>
            </p>
        </div>
    </main>

</body>

</html>