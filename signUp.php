<?php
include_once("config.php");
include_once("repository/database.php");
include_once("repository/UsuariosRepositorio.php");
include_once("repository/BancosRepositorio.php");
include_once("model/usuarios/Administrador.php");
include_once("model/usuarios/Cliente.php");
include_once('utils/menuNavegacion.php');

$ur = new UsuariosRepositorio();
if (!empty($_POST['nombre'])) {
  $ur->agregar(new Cliente(Usuario::ID, $_POST['nombre'], $_POST['apellido'], $_POST['email'], $_POST['email'], $_POST['password'], $_POST['banco']));
  header("Location: login.php");
} else {
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>SignUp</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="assets/loginYregistro/style.css">
  <link rel="stylesheet" type="text/css" href="assets/style.css">
</head>

<body>
  <?php
		echo menuNavegacion::getMenu(false);
	?>
  <main class="container">
    <?php if (!empty($message)) : ?>
      <p> <?= $message ?></p>
    <?php endif; ?>
    <h1>SignUp</h1>
    <form action="signUp.php" method="POST">
      <input name="nombre" type="nombre" placeholder="Ingresa tu nombre">
      <input name="apellido" type="apellido" placeholder="Ingresa tu apellido">
      <input name="email" type="email" placeholder="Ingresa tu correo">
      <input name="password" type="password" placeholder="Ingresa tu contraseña">
      <input name="confirm_password" type="password" placeholder="Confirma tu contraseña">
      <select name="banco">
      <?php
        $br = new BancosRepositorio();
        $html = "";
        foreach ($br->obtenerBancos() as $fila) {
          $html .= "<option value='" . $fila['ID'] . "'>" . $fila['Nombre'] . "</option>";
        }
        echo $html;
      ?>
      </select>
      <input type="submit" value="Submit">
    </form>
    <span><a href="login.php">Inicia Sesión</a></span>
    <span>o <a href="inicioVisitante.php">Ingresar como visitante</a></span>
  </main>
</body>

</html>