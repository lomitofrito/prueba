<?php
include_once('utils/menuNavegacion.php');
include_once("repository/ProductosRepositorio.php");
include_once("model/usuarios/Administrador.php");
include_once("model/banco/Producto.php");
include_once("model/banco/Credito.php");
include_once("config.php");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
    <title></title>
</head>

<body>
    <?php
    echo menuNavegacion::getMenu(false);
    if(isset($_POST['no'])){
        header("Location: pageTarjetaCredito.php");
    }
    ?>
    <main class="container">
        <?php
        $productosRepo = new ProductosRepositorio();
        $usuariosRepo = new UsuariosRepositorio();

        $usuario_actual = $usuariosRepo->getUsuarioActual(true);

        $resultado = $productosRepo->agregar(new TarjetaCredito(Producto::ID, $usuario_actual->id, 100, 110, 50, 10)); //TODO -> Todos los valores ingresados son ejemplos, se tiene que dar todos estos datos por el administrador
        if ($resultado) {
            echo "<p>Solicitud de tarjeta de crédito creada correctamente</p>";
        } else {
            echo "<p>Error en la Solicitud de la tarjeta de crédito, intente nuevamente<p>";
        }
        ?>
    </main>
</body>

</html>