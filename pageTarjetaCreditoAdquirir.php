<?php
include_once('utils/menuNavegacion.php');
include_once("repository/UsuariosRepositorio.php");
include_once("model/usuarios/Visitante.php");
include_once("model/usuarios/Usuario.php");
include_once("config.php");

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="assets/style.css">
	<link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
    <title></title>
</head>

<body>
    <?php
    echo menuNavegacion::getMenu(false);
    ?>
    <main class="container">
        <?php
        $ur = new UsuariosRepositorio();
        $usuario_actual = $ur->getUsuarioActual(true);

        $html = "";
        $html .= "<form action='pageTarjetaCreditoProcesar.php' method='POST'>";
        $html .= "<h2>¿Está seguro que desea solicitar una nueva tarjeta de crédito registrado a nombre de $usuario_actual->nombre ?</h2>";
        $html .= "<input type='submit' name='si' value='Si'>";
        $html .= "<input type='submit' name='no' value='No'>";
        $html .= "</form>";
        echo $html;
        ?>
    </main>
</body>

</html>