<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

include_once("vendor/autoload.php");
class Correo
{

    private $mail;
    private string $usuario;

    function __construct(string $usuario, string $password)
    {
        $this->usuario = $usuario;
        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        $this->mail->Host = 'smtp.gmail.com';
        //$this->mail->Host = gethostbyname('smtp.gmail.com');

        // para mostrar todo lo de debug
        // $this->mail->SMTPDebug = SMTP::DEBUG_SERVER;

        //Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $this->mail->SMTPSecure = 'tls';
        $this->mail->Port = 587;

        //Set the encryption mechanism to use - STARTTLS or SMTPS
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        //Whether to use SMTP authentication
        $this->mail->SMTPAuth = true;

        //Username to use for SMTP authentication - use full email address for gmail
        $this->mail->Username = $usuario;

        //Password to use for SMTP authentication
        $this->mail->Password = $password;


        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    }

    function send($whoto, $titulo, $mensaje)
    {

        //Set who the message is to be sent from
        $this->mail->setFrom($this->usuario, 'Correo de prueba');

        //Set an alternative reply-to address
        // $this->mail->addReplyTo('replyto@example.com', 'First Last');

        //Set who the message is to be sent to
        $this->mail->addAddress($whoto, $whoto);

        //Set the subject line
        $this->mail->Subject = $titulo;

        $this->mail->Body = $mensaje;

        //Attach an image file
        // $this->mail->addAttachment('images/phpmailer_mini.png');

        //send the message, check for errors
        if (!$this->mail->send()) {
            echo 'Mailer Error: ' . $this->mail->ErrorInfo;
        } else {
            echo 'Message sent!';
        }
    }
}
?>