<?php
include_once('repository/UsuariosRepositorio.php');
include_once('config.php');
class menuNavegacion
{
    static function getMenu($sesionYaIniciadaEnPagActual)
    {
        $ur = new UsuariosRepositorio();
        $usuario_actual = $ur->getUsuarioActual($sesionYaIniciadaEnPagActual);

        $menu = '';
        $menu .= "<div id='container'>";
        $menu .= "  <nav>";
        $menu .= "      <ul>";
        $menu .= "          <li><a href='index.php'>Inicio</a></li>";
        $menu .= "          <li><a href='#'>Productos<i class='down'></i></a>";
        $menu .= "              <ul>";
        $menu .= "                  <li><a href='pageCuentaAhorros.php'>Cuenta de ahorros</a></li>";
        $menu .= "                  <li><a href='pageCredito.php'>Crédito</a></li>";
        $menu .= "                  <li><a href='pageTarjetaCredito.php'>Tarjeta de crédito</a></li>";
        $menu .= "              </ul>";
        $menu .= "          </li>";

        if ($usuario_actual != null && get_class($usuario_actual) == 'Cliente') {
            $menu .= "          <li><a href='#'>Operaciones bancarias<i class='down'></i></a>";
            $menu .= "              <ul>";
            $menu .= "                  <li><a href='Retiros.php'>Retiros</a></li>";
            $menu .= "                  <li><a href='Consignaciones.php'>Consignación</a></li>";
            $menu .= "                  <li><a href='#'>Compra con tarjeta de crédito</a></li>";
            $menu .= "              </ul>";
            $menu .= "          </li>";
        }

        if ($usuario_actual != null && get_class($usuario_actual) == 'Administrador') {
            $menu .= "          <!--Este debe habilitarse solo para los usuarios admin-->";
            $menu .= "          <li><a href='#'>Administración<i class='down'></i></a>";
            $menu .= "              <ul>";
            $menu .= "                  <li><a href='#'>Cambios en Usuarios</a></li>";
            $menu .= "                  <li><a href='#'>Definir cuotas de manejo</a></li>";
            $menu .= "                  <li><a href='#'>Centro de mensajes</a></li>";
            $menu .= "                  <li><a href='#'>Fin de mes</a>";
            $menu .= "                      <ul>";
            $menu .= "                          <li><a href='#'>#1</a></li>";
            $menu .= "                          <li><a href='#'>#2</a></li>";
            $menu .= "                      </ul>";
            $menu .= "                  </li>";
            $menu .= "              </ul>";
            $menu .= "          </li>";
        }

        $menu .= "          <li class='izquierda'><a href='menuPruebas.php'>Pruebas<i class='down'></i></a>";
        $menu .= "              <ul>";
        $menu .= "                  <li><a href='#'>Antes de iniciar</a>";
        $menu .= "                      <ul>";
        $menu .= "                          <li><a href='iniciarServidor.php'>Iniciar Servidor</a></li>";
        $menu .= "                      </ul>";
        $menu .= "                  </li>";
        $menu .= "                  <li><a href='#'>Principales</a>";
        $menu .= "                      <ul>";
        $menu .= "                          <li><a href='signUp.php'>Sign Up (Registrar Usuario)</a></li>";
        $menu .= "                          <li><a href='login.php'>Login</a></li>";
        $menu .= "                          <li><a href='logout.php'>Logout</a>";
        $menu .= "                      </ul>";
        $menu .= "                  </li>";
        $menu .= "                  <li><a href='#'>Auxiliares</a>";
        $menu .= "                      <ul>";
        $menu .= "                          <li><a href='pruebaCorreo.php'>Prueba Correo</a></li>";
        $menu .= "                      </ul>";
        $menu .= "                  </li>";
        $menu .= "              </ul>";
        $menu .= "          </li>";

        if ($usuario_actual == null || get_class($usuario_actual) == 'Visitante') {
            $menu .= "      <li class='derecha'><a href='login.php'>Iniciar Sesión</a></li>";
            $menu .= "      <li class='derecha'><a href='signUp.php'>Registrarse</a></li>";
        } else {
            $menu .= "      <li class='derecha'><a href='#'>Bienvenido $usuario_actual->nombre <i class='down'></i></a>";
            $menu .= "          <ul>";
            $menu .= "          	<li class='derecha'><a href='logout.php'>Cerrar Sesión</a></li>";
            $menu .= "          </ul>";
            $menu .= "      </li>";
        }

        $menu .= "      </ul>";
        $menu .= "   </nav>";
        $menu .= "</div>";
        return $menu;
    }
}
?>