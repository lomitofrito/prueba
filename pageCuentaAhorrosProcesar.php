<?php
include_once('utils/menuNavegacion.php');
include_once("repository/ProductosRepositorio.php");
include_once("model/usuarios/Administrador.php");
include_once("model/banco/Producto.php");
include_once("model/banco/Credito.php");
include_once("config.php");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <link rel="stylesheet" type="text/css" href="assets/loginYregistro/style.css">
    <title></title>
</head>

<body>
    <?php
    echo menuNavegacion::getMenu(false);
    if(isset($_POST['no'])){
        header("Location: pageCuentaAhorros.php");
    }
    ?>
    <main class="container">
        <?php
        $productosRepo = new ProductosRepositorio();
        $usuariosRepo = new UsuariosRepositorio();

        $usuario_actual = $usuariosRepo->getUsuarioActual(true);

        $resultado = $productosRepo->agregar(new CuentaAhorro(Producto::ID, $usuario_actual->id, 0, 20)); //TODO -> la cuota de manejo se deberá poner la que pone el administrador en este banco
        if ($resultado) {
            echo "<p>Cuenta de Ahorros creada correctamente</p>";
        } else {
            echo "<p>Error en la solicitud Cuenta de Ahorros, intente nuevamente<p>";
        }
        ?>
    </main>
</body>

</html>