<?php
include_once('repository/database.php');
include_once('model/banco/Transaccion.php');
class TransaccionRepositorio
{
    private Database $con;

    public function __construct(Database $db)
    {
        // $this->con = new Database($host, $usuario, $password, $database);
        $this->con = $db;
    }
    
    function agregar(Transaccion $transaccion)
    {
        $sql  = "INSERT INTO Transaccion" .
              "(ID, id_producto_origen, id_producto_destino, monto, cuotas, tipo, aprovado)".
              "VALUES (";

        $sql .= $transaccion->ID;
        $sql .= $transaccion->id_producto_origen  . ", ";
        $sql .= $transaccion->id_producto_destino . ", ";
        $sql .= $transaccion->monto               . ", ";
        $sql .= $transaccion->cuotas              . ", ";
        $sql .= $transaccion->tipo                . ", ";
        $sql .= $transaccion->aprovado            . ", ";
        $sql .= ")";
        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error insertando a " . ": " . $this->con->error();
            return false;
        }
    }

    function actualizar(Transaccion $transaccion)
    {
        $sql  = "UPDATE Transaccion ";
        $sql .= "SET ";
        $sql .= "id_producto_origen  = " . $transaccion->id_producto_origen  . ", ";
        $sql .= "id_producto_destino = " . $transaccion->id_producto_destino . ", ";
        $sql .= "monto               = " . $transaccion->monto               . ", ";
        $sql .= "cuotas              = " . $transaccion->cuotas              . ", ";
        $sql .= "tipo                = " . $transaccion->tipo                . ", ";
        $sql .= "aprovado            = " . $transaccion->aprovado            . ", ";
        $sql .= "WHERE ID            = " . $transaccion->ID;

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error actualizando transaccion " . $transaccion->ID . ": " . $this->con->error();
            return false;
        }
    }

    function eliminar($id)
    {
        $this->con->eliminar('Transaccion', $id);
    }

    function buscarTransaccionID($id)
    {
        $this->con->buscar('Transaccion', 'ID', $id);
    }

    static function table_def()
    {
        $sql = "CREATE TABLE Transaccion
        (
            ID                  INT    NOT NULL AUTO_INCREMENT, PRIMARY KEY(ID),
            id_producto_origen  INT    NOT NULL,
            FOREIGN KEY (id_producto_origen) REFERENCES Productos(ID),
            id_producto_destino INT    NOT NULL,
            FOREIGN KEY (id_producto_destino) REFERENCES Productos(ID),
            monto               NUMERIC NOT NULL,
            cuotas              INT    NOT NULL,
            tipo                VARCHAR(20) NOT NULL,
            aprovado            BOOLEAN NOT NULL
        )";

        return $sql;
    }
}
?>
