<?php
include_once('repository/database.php');
include_once('model/usuarios/Administrador.php');
include_once('model/usuarios/Cliente.php');
include_once('model/usuarios/Visitante.php');
include_once("config.php");
class UsuariosRepositorio
{
    private $con;

    public function __construct()
    {
        $this->con = new Database(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);
    }

    function agregar($usuario)
    {
        $user = ($usuario->user == "NULL" ? $usuario->user : "'" . $usuario->user . "'");
        $password = ($usuario->password == "NULL" ? $usuario->password : "'" . $usuario->password . "'");
        $tipo = get_class($usuario);

        $sql  = "INSERT INTO Usuarios (Nombre            , Apellido            , Correo            , User , Password , Tipo   , Id_banco           ) 
                               VALUES ('$usuario->nombre', '$usuario->apellido', '$usuario->correo', $user, $password, '$tipo', $usuario->id_banco )";

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error insertando a " . $usuario->nombre . " " . $usuario->apellido . ": " . $this->con->error();
            return false;
        }
    }

    function actualizar(Usuario $usuario)
    {
        $sql  = "UPDATE Usuarios ";
        $sql .= "SET Nombre = '" . $usuario->nombre    . "', ";
        $sql .= "Apellido = '"   . $usuario->apellido  . "', ";
        $sql .= "Correo = '"     . $usuario->correo    . "', ";
        $sql .= "User = '"       . $usuario->user      . "', ";
        $sql .= "Password = '"   . $usuario->password  . "', ";
        $sql .= "Tipo = '"       . get_class($usuario) . "', ";
        $sql .= "WHERE ID = "    . $usuario->id;

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error actualizando a " . $usuario->nombre . " " . $usuario->apellido . ": " . $this->con->error();
            return false;
        }
    }

    function eliminar($id)
    {
        $this->con->eliminar('Usuarios', $id);
    }

    function obtener_producto_por_tipo_y_id_usuario($tipoProducto, $id)
    {
        $sql  = "SELECT p.* ";
        $sql .= "FROM Usuarios u, Productos p ";
        $sql .= "WHERE u.ID   = p.Id_cliente and ";
        $sql .=       "u.ID   = $id and ";
        $sql .=       "p.Tipo = '$tipoProducto'";

        $resultado = $this->con->query($sql);
        if ($resultado != null) {
            $rows = array();
            while ($result = $resultado->fetch_array(MYSQLI_BOTH)) {       
                $rows[] = $result;
            }
            return $rows;
        } else {
            //echo "Error buscando en tabla ". $tabla ." elemento con " . $atributo . ": " . $valor . " -> " . $this->error();
            return null;
        }
    }

    function obtener_usuario_por_id($id)
    {
        return $this->buscar_por("ID", $id);
    }

    function obtener_usuario_por_user($user)
    {
        return $this->buscar_por("User", $user);
    }

    function obtener_usuario_por_correo($email)
    {
        return $this->buscar_por("Correo", $email);
    }

    private function buscar_por($atributo, $valor)
    {
        $resultado = $this->con->buscar('Usuarios', $atributo, $valor);
        if ($resultado != null) {
            return $this->obtener_usuario($resultado);
        }
        return null;
    }

    private function obtener_usuario($resultado)
    {
        $fila = mysqli_fetch_array($resultado);
        if ($fila['Tipo'] == "Cliente") {
            return new Cliente($fila["ID"], $fila["Nombre"], $fila["Apellido"], $fila["Correo"], $fila["User"], $fila["Password"], $fila['Id_banco']);
        } else if ($fila['Tipo'] == "Administrador") {
            return new Administrador($fila["ID"], $fila["Nombre"], $fila["Apellido"], $fila["Correo"], $fila["User"], $fila["Password"], $fila['Id_banco']);
        } else {
            return new Visitante($fila["ID"], $fila["Nombre"], $fila["Apellido"], $fila["Correo"], $fila['Id_banco']); 
        }
    }

    static function table_def()
    {
        $sql = "CREATE TABLE Usuarios 
        (
            ID         INT      NOT NULL AUTO_INCREMENT, 
            Nombre     CHAR(20) NOT NULL,
            Apellido   CHAR(20) NOT NULL,
            Correo     CHAR(50) NOT NULL,
            User       CHAR(50) NULL UNIQUE,
            Password   CHAR(50) NULL,
            /*Encargado de describir si es cliente, administrador o visitante*/
            Tipo       CHAR(50) NOT NULL, 
            Id_banco   INT NOT NULL,
            PRIMARY KEY (ID),
            FOREIGN KEY (Id_banco) REFERENCES Bancos(ID)
        )";

        return $sql;
    }

    function getUsuarioActual($sesionYaIniciadaEnPagActual)
    {
        if(!$sesionYaIniciadaEnPagActual)
            session_start();
        if (isset($_SESSION['userID'])) {
            $usuario_encontrado = $this->obtener_usuario_por_id($_SESSION['userID']);
            return $usuario_encontrado;
        } else {
            return null;
        }
    }
}
?>
