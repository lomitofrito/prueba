<?php
include_once('repository/database.php');
include_once('model/banco/Banco.php');
include_once("config.php");
class ComprasRepositorio
{
    private Database $con;

    public function __construct()
    {
        $this->con = new Database(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);
    }

    function agregar($compra)
    {
        $sql  = "INSERT INTO Compras (Valor         , Cuotas         , Moneda         , Id_tarjeta_credito         ) 
                             VALUES  ($compra->valor, $compra->cuotas, $compra->moneda, $compra->id_tarjeta_credito)";

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error insertando Compra con valor de " . $compra->valor . ": " . $this->con->error();
            return false;
        }
    }

    function eliminar($id)
    {
        $this->con->eliminar('Compras', $id);
    }

    function buscarBancoID($id)
    {
        $this->buscar_por('ID', $id);
    }

    private function buscar_por($atributo, $valor)
    {
        $resultado = $this->con->buscar('Compras', $atributo, $valor);
        if ($resultado != null) {
            $fila = mysqli_fetch_array($resultado);
            return new Banco($fila["ID"], $fila["Id_cliente"], $fila["Nombre"]);
        }
        return null;
    }
    
    static function table_def()
    {

        $sql = "CREATE TABLE Compras
        (
            ID                 INT NOT NULL AUTO_INCREMENT,
            Valor              INT NOT NULL,
            Cuotas             DECIMAL(15,2) NOT NULL,
            Moneda             CHAR(50) NOT NULL,
            Id_tarjeta_credito INT NOT NULL,
            PRIMARY KEY (ID),
            FOREIGN KEY (Id_tarjeta_credito) REFERENCES Producto(ID)
        )";

        return $sql;
    }
}
?>
