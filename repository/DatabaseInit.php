<?php
include_once("repository/ProductosRepositorio.php");
include_once("repository/UsuariosRepositorio.php");
include_once("repository/MensajesRepositorio.php");
include_once("repository/database.php");
include_once("repository/TransaccionRepositorio.php");
include_once("repository/ComprasRepositorio.php");
include_once("config.php");

class DatabaseInit
{
    private $connection;

    function __construct()
    {
        //Crear base de datos
        $this->create_database(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);

        //Crear la conexión ya a la base de datos creada
        $this->connection = new mysqli(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);
    }

    function create_database($host, $username, $password, $dbname)
    {
        $this->connection = new mysqli($host, $username, $password);

        //Comprobar la Conexión
        if ($this->error()) {
            echo "Error en la conexion: " . $this->error() . "<cr>";
        }

        //SQL
        $sql = "CREATE DATABASE " . $dbname;

        //Confirmación
        if ($this->query($sql) === TRUE) {
            echo "Base de datos " . $dbname . " creada correctamente\n";
        } else {
            echo "Error en la creacion de Base de datos " . $dbname . ": " . $this->error();
        }

        //Cerrar esta conexión para conectarse dentro de la base de datos
        $this->close();
    }

    function create_table($nombre, $sql)
    {
        if ($this->query($sql) === TRUE) {
            echo "Tabla " . $nombre . " creada correctamente\n";
        } else {
            echo "Error en la creacion de tabla " . $nombre . ": " . $this->error() ."\n";
        }
    }

    function create_tables()
    {
        $this->create_table('Bancos', BancosRepositorio::table_def());
        $this->create_table('Compras', ComprasRepositorio::table_def());
        $this->create_table('Usuarios',   UsuariosRepositorio::table_def());
        $this->create_table('Productos', ProductosRepositorio::table_def());
		$this->create_table('Mensajes',   MensajesRepositorio::table_def());
    }

    function query($sql)
    {
        return $this->connection->query($sql);
    }

    function error()
    {
        return $this->connection->connect_error;
    }

    function close()
    {
        $this->connection->close();
    }

    function __destruct()
    {
        $this->close();
    }
}
?>
