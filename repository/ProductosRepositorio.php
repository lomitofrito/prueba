<?php
include_once('repository/database.php');
include_once('model/banco/Producto.php');
include_once('model/banco/CuentaAhorro.php');
include_once('model/banco/Credito.php');
include_once('model/banco/TarjetaCredito.php');
include_once("config.php");
class ProductosRepositorio
{
    private Database $con;

    public function __construct()
    {
        $this->con = new Database(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);
    }

    function agregar($producto)
    {
        $fecha_pago = ($producto->fecha_pago == "NULL" ? $producto->fecha_pago : "'" . $producto->fecha_pago . "'");
        $tipo = get_class($producto);
        $aprobado = "";
        if($producto->aprobado == "NULL"){
            $aprobado =  $producto->aprobado;
        } else {
            $aprobado =  ($producto->aprobado == true ? 1 : 0);
        }
        $sql  = "INSERT INTO Productos (Id_cliente           , Saldo           , Cuota_manejo           , Fecha_pago , Aprobado , Tasa_interes           , Cupo_max           , Sobrecupo           , Tipo   ) 
                                VALUES ($producto->id_cliente, $producto->saldo, $producto->cuota_manejo, $fecha_pago, $aprobado, $producto->tasa_interes, $producto->cupo_max, $producto->sobrecupo, '$tipo')";

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error insertando Producto " . $producto->id . ": " . $this->con->error();
            return false;
        }
    }

    function actualizar($producto)
    {
        $sql  = "UPDATE Productos ";
        $sql .= "SET ";
        $sql .= "Id_cliente = "   . $producto->id_cliente . ", ";
        $sql .= "Saldo = "        . $producto->saldo . ", ";
        $sql .= "Cuota_manejo = " . $producto->cuota_manejo . ", ";
        $sql .= "Fecha_pago = "   . $producto->fecha_pago . ", ";
        $sql .= "Aprobado = "     . $producto->aprobado . ", ";
        $sql .= "Tasa_interes = " . $producto->tasa_interes . ", ";
        $sql .= "Cupo_max = "     . $producto->cupo_max . ", ";
        $sql .= "Sobrecupo = "    . $producto->sobrecupo . ", ";
        $sql .= "Tipo = "         . get_class($producto) . ", ";
        $sql .= "WHERE ID = " . $producto->id;

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error actualizando Producto " . get_class($producto) . " con id " . $producto->id . ": " . $this->con->error();
            return false;
        }
    }

    function eliminar($id)
    {
        $this->con->eliminar('Productos', $id);
    }

    function buscarProductoID($id)
    {
        $this->buscar_por('ID', $id);
    }

    private function buscar_por($atributo, $valor)
    {
        $resultado = $this->con->buscar('Productos', $atributo, $valor);
        if ($resultado != null) {
            return $this->obtener_producto($resultado);
        }
        return null;
    }

    private function obtener_producto($resultado)
    {
        $fila = mysqli_fetch_array($resultado);
        if ($fila['Tipo'] == "CuentaAhorro") {
            return new CuentaAhorro($fila["ID"], $fila["Id_cliente"], $fila["Saldo"], $fila["Cuota_manejo"]);
        } else if ($fila['Tipo'] == "Credito") {
            return new Credito($fila["ID"], $fila["Id_cliente"], $fila["Saldo"], $fila["Fecha_pago"], $fila["Aprobado"], $fila["Tasa_interes"]);
        } else {
            return new TarjetaCredito($fila["ID"], $fila["Id_cliente"], $fila["Cupo_max"], $fila["Sobrecupo"], $fila["Cuota_manejo"], $fila["Tasa_interes"]);
        }
    }

    static function table_def()
    {
        $sql = "CREATE TABLE Productos
        (
            ID           INT NOT NULL AUTO_INCREMENT,
            Id_cliente   INT NOT NULL,
            Saldo        DECIMAL(15,2) NULL,
            Cuota_manejo DECIMAL(15,2) NULL,
            Fecha_pago   DATE NULL,
            Aprobado     TINYINT(1) NULL,
            Tasa_interes INT NULL,
            Cupo_max     DECIMAL(15,2) NULL,
            Sobrecupo    DECIMAL(15,2) NULL,
            Tipo         CHAR(50) NOT NULL,
            PRIMARY KEY (ID),
            FOREIGN KEY (Id_cliente) REFERENCES Usuarios(ID)
        )";

        return $sql;
    }
}
?>
