<?php
include_once('repository/database.php');
include_once('model/banco/Banco.php');
include_once("config.php");
class BancosRepositorio
{
    private Database $con;

    public function __construct()
    {
        $this->con = new Database(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);
    }

    function agregar($banco)
    {
        $sql  = "INSERT INTO Bancos ( Nombre         ) 
                             VALUES ('$banco->nombre')";

        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error insertando Banco " . $banco->nombre . ": " . $this->con->error();
            return false;
        }
    }

    function eliminar($id)
    {
        $this->con->eliminar('Bancos', $id);
    }

    function buscarBancoID($id)
    {
        $this->buscar_por('ID', $id);
    }

    private function buscar_por($atributo, $valor)
    {
        $resultado = $this->con->buscar('Bancos', $atributo, $valor);
        if ($resultado != null) {
            $fila = mysqli_fetch_array($resultado);
            return new Banco($fila["ID"], $fila["Nombre"]);
        }
        return null;
    }

    function obtenerBancos()
    {
        $sql = "SELECT * FROM Bancos";

        $resultado = $this->con->query($sql);
        if ($resultado != null) {
            $rows = array();
            while ($result = $resultado->fetch_array(MYSQLI_BOTH)) {       
                $rows[] = $result;
            }
            return $rows;
        } else {
            //echo "Error buscando en tabla ". $tabla ." elemento con " . $atributo . ": " . $valor . " -> " . $this->error();
            return null;
        }
    }
    
    static function table_def()
    {

        $sql = "CREATE TABLE Bancos
        (
            ID     INT NOT NULL AUTO_INCREMENT,
            Nombre CHAR(50) NOT NULL,
            PRIMARY KEY (ID)
        )";

        return $sql;
    }
}
?>
