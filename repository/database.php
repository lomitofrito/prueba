<?php
class Database
{
    private $con;

    function __construct($host, $username, $password, $dbname)
    {
        $this->con = new mysqli($host, $username, $password, $dbname);

        if ($this->con->connect_errno) {
            echo "Error en la conexion: " . $this->con->connect_error . "<cr>";
        }
    }

    
    function eliminar($tabla, $id)
    {
        $sql = "DELETE FROM ". $tabla ." WHERE ID = " . $id;

        if ($this->query($sql)) {
            return true;
        } else {
            echo "Error Eliminando de tabla ". $tabla ." elemento con id " . $id . ": " . $this->error();
            return false;
        }
    }

    function buscar($tabla, $atributo, $valor)
    {
        $sql = "SELECT * FROM ". $tabla ." WHERE " . $atributo . " = '" . $valor."'";
        $resultado = $this->query($sql);
        if ($resultado->num_rows > 0) {
            return $resultado;
        } else {
            //echo "Error buscando en tabla ". $tabla ." elemento con " . $atributo . ": " . $valor . " -> " . $this->error();
            return null;
        }
    }

    function query($sql)
    {
        return $this->con->query($sql);
    }

    function error()
    {
        return $this->con->connect_error;
    }

    function close()
    {
        $this->con->close();
    }

    static function eliminar_Database()
    {
        $con = new mysqli(HOST_DB, USUARIO_DB, USUARIO_PASS);
        $sql = "DROP DATABASE " . NOMBRE_DB;

        if ($con->query($sql)) {
            echo "Base de datos " . NOMBRE_DB . " eliminada correctamente";
            return true;
        } else {
            echo "Error en la eliminación de la base de datos " . NOMBRE_DB . ": " . $con->connect_error;
            return false;
        }
    }

    function __destruct()
    {
        $this->close();
    }
}
?>