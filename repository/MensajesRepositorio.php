<?php
include_once('repository/database.php');
include_once('model/usuarios/Mensaje.php');
class MensajesRepositorio
{
    private Database $con;

    public function __construct(Database $db)
    {
        // $this->con = new Database($host, $usuario, $password, $database);
        $this->con = $db;
    }

    function agregar(Mensaje $mensaje)
    {
        $sql  = "INSERT INTO Mensajes" .
              "(ID, id_producto_origen, id_producto_destino, monto, cuotas, tipo, aprovado)".
              "VALUES (";

        $sql .= $$mensaje->id;
        $sql .= $$mensaje->id2  				. ", ";
        $sql .= $$mensaje->asunto 				. ", ";
        $sql .= $$mensaje->user1                . ", ";
        $sql .= $$mensaje->user2                . ", ";
        $sql .= $$mensaje->mensaje              . ", ";
        $sql .= $$mensaje->timestamp            . ", ";
		$sql .= $$mensaje->user1read            . ", ";
		$sql .= $$mensaje->user2read            . ", ";
        $sql .= ")";
        if ($this->con->query($sql)) {
            return true;
        } else {
            echo "Error insertando a " . ": " . $this->con->error();
            return false;
        }
    }

    function eliminar($id)
    {
        $this->con->eliminar('Mensajes', $id);
    }

    function buscarMensajeID($id)
    {
        $this->con->buscar('Mensajes', 'id', $id);
    }

    static function table_def()
    {
       		
		$sql = "CREATE TABLE Mensajes (
		  id bigint(20) NOT NULL,
		  id2 int(11) NOT NULL,
		  asunto varchar(256) NOT NULL,
		  user1 bigint(20) NOT NULL,
		  user2 bigint(20) NOT NULL,
		  mensaje text NOT NULL,
		  timestamp int(10) NOT NULL,
		  user1read varchar(3) NOT NULL,
		  user2read varchar(3) NOT NULL
		)";
        return $sql;
    }
}
?>
