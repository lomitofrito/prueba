<?php
include_once('Producto.php');
include_once('repository/database.php');
class TarjetaCredito extends Producto
{
    public $cupo_max;
    public $sobrecupo;
    public $cuota_manejo;
    public $tasa_interes;

    public $saldo      = "NULL";
    public $fecha_pago = "NULL";
    public $aprobado   = "NULL";

    function __construct(
        $id,
        $id_cliente,
        $cupo_max,
        $sobrecupo,
        $cuota_manejo,
        $tasa_interes
    ) {
        parent::__construct($id, $id_cliente);
        $this->cupo_max     = $cupo_max;
        $this->sobrecupo    = $sobrecupo;
        $this->cuota_manejo = $cuota_manejo;
        $this->tasa_interes = $tasa_interes;
        $this->id_cliente    = $id_cliente;
    }

    // realizar una compra con tc
    function compra()
    {
    }

    // pagar las "deudas" conseguidas por compras con tarjetas de credito
    // esto se hace a final de mes
    function pago()
    {
    }

    function cobraCuotaManejo()
    {
    }
}
?>
