<?php
include_once('model/banco/Producto.php');
include_once('repository/ProductosRepositorio.php');
class Credito extends Producto
{
    public $fecha_pago;
    public $aprobado;
    public $tasa_interes;
    public $saldo;

    public $cuota_manejo = "NULL";
    public $cupo_max     = "NULL";
    public $sobrecupo    = "NULL";

    function __construct(
        $id,
        $id_cliente,
        $saldo,
        $fecha_pago,
        $aprobado,
        $tasa_interes
    ) {
        parent::__construct($id, $id_cliente);
        $this->fecha_pago   = $fecha_pago;
        $this->aprobado     = $aprobado;
        $this->tasa_interes = $tasa_interes;
        $this->saldo        = $saldo;
    }

    // aumentar cantidad javecoins
    function consignacion()
    {
    }

    // cobrar un credito a final de mes
    function cobrarCredito()
    {
    }
}
?>
