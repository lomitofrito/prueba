<?php
include_once('Producto.php');
include_once('repository/database.php');
// Esta clase se encarga de modificar los registros de un producto
// en un banco especifico
class CuentaAhorro extends Producto
{
    public $cuota_manejo;
    public $saldo;

    public $fecha_pago   = "NULL";
    public $aprobado     = "NULL";
    public $tasa_interes = "NULL";
    public $cupo_max     = "NULL";
    public $sobrecupo    = "NULL";

    function __construct(
        $id,
        $id_cliente,
        $saldo,
        $cuota_manejo
    ) {
        parent::__construct($id, $id_cliente);
        $this->cuota_manejo = $cuota_manejo;
        $this->saldo        = $saldo;
    }

    // reducir cantidad javecoins
    function retiro()
    {
    }

    // aumentar cantidad javecoins
    function consignacion()
    {
    }

    // incrementar el saldo de las cuentas de acuerdo a intereses definidos
    function incrementarSaldo()
    {
    }

    function cobraCuotaManejo()
    {
    }
}
?>
