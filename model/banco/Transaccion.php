<?php
include_once('repository/TransaccionRepositorio.php');
class Transaccion
{
    public $ID;
    public $id_producto_origen;
    public $id_producto_destino;
    public $monto;
    public $cuotas;
    public $tipo;
    public $aprovado;

    function __constructor(
        $ID,
        $id_producto_origen,
        $id_producto_destino,
        $monto,
        $cuotas,
        $tipo,
        bool $aprovado
    )
    {

        $this->ID                  = $ID;
        $this->id_producto_origen  = $id_producto_origen;
        $this->id_producto_destino = $id_producto_destino;
        $this->monto               = $monto;
        $this->cuotas              = $cuotas;
        $this->tipo                = $tipo;
        $this->aprovado            = $aprovado;
    }

    function commit($db)
    {
        $tr = new TransaccionRepositorio($db);
        return $tr->agregar($this);
    }
}

?>
