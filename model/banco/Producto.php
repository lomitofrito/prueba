<?php
include_once('model/banco/Transaccion.php');
include_once('repository/TransaccionRepositorio.php');
include_once('repository/database.php');
abstract class Producto
{
    const ID = 0;
    public $id;
    public $id_cliente;

    function __construct(
        $id,
        $id_cliente
    ) {
        $this->id          = $id;
        $this->id_cliente  = $id_cliente;
    }

    // crear transaccion 1 y 2
    // modificar producto  -- en caso de tener problemas con transacciones, no se crean
    // modificar producto 2
    // -- en caso de tener problemas en modificacion productos, se eliminan transacciones
    function modificarRegistroBancario (
        Database $db1, // banco 1
        Database $db2, // banco 2
        int $id_producto_destino,
        float $monto,
        int $cuotas,
        string $tipo
    )
    {
        $id_transaccion = 0; // TODO: no estoy completamente seguro de como generar esto
                        // tal vez hacer que se genere a partir de la bd y que se retorne
                        // despues de la creacion;
        // primero se tiene que crear la transaccion
        $t = new Transaccion(
            $id_transaccion,
            $this->ID,
            $id_producto_destino,
            $monto,
            $cuotas,
            $tipo,
            false
        );

        // en caso de ser iguales guarda el primero, de lo contrario, guarda los dos
        $res = $t->commit($db1) &&
             (
                 $db1 === $db2
                 ||
                 $t->commit($db2)
             )
             ;

        // se modifica la cuenta si la creacion de la transaccion fue exitosa
        if ($res)
        {
            // esta es una funcion abstracta en la que se modifica el
            // valor del producto en la base de datos
            // $resultado = this->modificarProducto($db1);
        }
        else
        {
            echo 'ha ocurrido un error en la transaccion';
            // se elimina la transaccion en caso de tener un problema en la modificacion
            // es mas facil deshacer la transaccion que la modificacion al producto
            // TODO: revisar esto

            $transaccion.eliminar($id_transaccion);
            $transaccion2.eliminar($id_transaccion);
        }

    }

    function modificarProducto (float $modificacion)
    {
        $cr = new ProductosRepositorio(HOST_DB, USUARIO_DB, USUARIO_PASS, NOMBRE_DB);
        $this->valor += $modificacion;
        $cr->actualizar($this);
    }
}
