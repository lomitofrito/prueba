<?php
include_once('Usuario.php');
class Cliente extends Usuario
{
    public $user;
    public $password;

    public function __construct($id, $nombre, $apellido, $correo, $user, $password, $id_banco)
    {
        parent::__construct($id, $nombre, $apellido, $correo, $id_banco);
        $this->user = $user;
        $this->password = $password;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function getID()
    {
        return parent::getId();
    }
}
?>