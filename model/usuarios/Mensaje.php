<?php
include_once('repository/MensajesRepositorio.php');
class Mensaje
{
    public $id;
    public $id2;
    public $asunto;
    public $user1;
    public $user2;
    public $mensaje;
    public $timestamp;
	public $user1read;
	public $user2read;
	


    function __constructor(
     $id,
     $id2,
     $asunto,
     $user1,
     $user2,
     $mensaje,
     $timestamp,
	 $user1read,
	 $user2read
    )
    {

        $this->id			=	$id;
     	$this->id2			=	$id2;
    	$this->asunto		=	$asunto;
	    $this->user1		=	$user1;
	    $this->user2		=	$user2;
	    $this->mensaje		=	$mensaje;
	    $this->timestamp	=	$timestamp;
		$this->user1read	=	$user1read;
		$this->user2read	=	$user2read;
    }

    function commit($db)
    {
        $tr = new MensajesRepositorio($db);
        return $tr->agregar($this);
    }
}




?>