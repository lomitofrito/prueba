<?php
include_once("repository/UsuariosRepositorio.php");

class Usuario
{
    const ID = 0;
    public $id;
    public $nombre;
    public $apellido;
    public $correo;
    public $id_banco;

    public function __construct($id, $nombre, $apellido, $correo, $id_banco)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->correo = $correo;
        $this->id_banco = $id_banco;
    }

    public function getID()
    {
        return $this->id;
    }

    public function obtenerCreditos()
    {
        $ur = new UsuariosRepositorio();
        return $ur->obtener_producto_por_tipo_y_id_usuario('Credito', $this->id);
    }

    public function obtenerCuentasAhorros()
    {
        $ur = new UsuariosRepositorio();
        return $ur->obtener_producto_por_tipo_y_id_usuario('CuentaAhorro', $this->id);
    }

    public function obtenerTarjetasCredito()
    {
        $ur = new UsuariosRepositorio();
        return $ur->obtener_producto_por_tipo_y_id_usuario('TarjetaCredito', $this->id);
    }

    public function tieneCreditos()
    {
        if($this->obtenerCreditos() == null){
            return false;
        } else {
            return true;
        }
    }

    public function tieneCuentasAhorros()
    {
        if($this->obtenerCuentasAhorros() == null){
            return false;
        } else {
            return true;
        }
    }

    public function tieneTarjetasCredito()
    {
        if($this->obtenerTarjetasCredito() == null){
            return false;
        } else {
            return true;
        }
    }
}
?>